#include<conio.h>
#include<iostream>
#include<stdio.h>
#include "cuda.h"
#include "cuda_runtime.h"
#include<device_launch_parameters.h>
__global__ void add(const float *a,const float *b,float *c,const size_t n ){
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	if (i < n) {
		c[i] = a[i] + b[i];
	
	}
}
int main() {

	const int n = 3;
	const int  floatsize = sizeof(float);
	float *d_a = 0;
	float *d_b = 0;
	float *d_c = 0;
	float *h_a = 0;
	float *h_b = 0;
	float *h_c = 0;
	///������ ���� ��� �� cpu
	h_a = (float*)malloc(floatsize);
	h_b = (float*)malloc(floatsize);
	h_c = (float*)malloc(floatsize);
	//������ ���� ��� �� GPU
	cudaMalloc((void**)&d_a, floatsize);
	cudaMalloc((void**)&d_b, floatsize);
	cudaMalloc((void**)&d_c, floatsize);
	if (d_a == 0 || d_b == 0 || d_c == 0 || h_a == 0 || h_b == 0 || h_c == 0) {
		printf("over flow could not allocate memory \n");
	}
	h_a[0] = 3;
	h_a[1] = 1;
	h_a[2] = 4;
	h_b[0] = 8;
	h_b[1] = 2;
	h_b[2] = 4;

	cudaMemcpy(d_a, h_a, floatsize, cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, h_b, floatsize, cudaMemcpyHostToDevice);
	
	const size_t blocksize = 3;
	size_t gridsize = n / blocksize;
	add << <(gridsize, blocksize) >> > (d_a, d_b, d_c, n)
		cudaMemcpy(h_c, d_c, floatsize, cudaMemcpyDeviceToHost);
	free(h_a);
	free(h_b);
	free(h_c);
	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_c);
	getch();
	return 0;
}